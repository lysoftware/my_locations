import React from 'react';
import AppWrapper from '../AppWrapper';
import {Container} from 'react-bootstrap';
import {CATEGORIES} from '../../consts';
import {connect} from 'react-redux';
import CategoryContainer from '../../containers/Category';
import './CategoriesView.css';


const MapStateToProps = state => {
    return {
        AllCategories: state.AllCategories,
    }
}


const CategoriesView = props => {
    return (
        <AppWrapper
            listType={CATEGORIES}
        >
            <Container id='CategoriesView'>
                {props.AllCategories.map(item => (
                    <CategoryContainer
                        key={'_' + item.name}
                        name={item.name}
                    />
                ))}
            </Container>
        </AppWrapper>
    );
}

export default connect(MapStateToProps)(CategoriesView);