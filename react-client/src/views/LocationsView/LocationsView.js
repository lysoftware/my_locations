import React from 'react';
import AppWrapper from '../AppWrapper';
import {Container, Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import {LOCATIONS} from '../../consts';
import {getLocationsByCategory, sortLocationsByName} from '../../utils';
import './LocationsView.css';
import LocationContainer from '../../containers/Location';


const MapStateToProps = state => {
    return {
        AllLocations: state.AllLocations,
        CurrentCategory: state.CurrentCategory,
    }
}


const LocationsView = props => {
    const GetStatusMessage = () => {
        if (!props.CurrentCategory) {
            return 'No category selected yet. please go to categories and select one';
        } else if (!getLocationsByCategory(props.CurrentCategory, Array.from(props.AllLocations)).length) {
            return 'No Locations for current category. You may add one.'
        }

        return null;
    }
    const GetProperLocations = () => {
        const LocationsByCategory = getLocationsByCategory(props.CurrentCategory, Array.from(props.AllLocations));
        return sortLocationsByName(LocationsByCategory);
    }
    return (
        <AppWrapper
            listType={LOCATIONS}
        >
            <Container id='LocationsView'>
                <Row>
                    <Col>
                        {GetStatusMessage()}
                    </Col>
                </Row>
                {GetProperLocations().map(item => (
                    <LocationContainer
                        key={'_' + item.name}
                        name={item.name}
                    />
                ))}
            </Container>
        </AppWrapper>
    );
}

export default connect(MapStateToProps)(LocationsView)