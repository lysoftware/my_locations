import React from 'react';
import {ADD_ITEM, CATEGORIES, LOCATIONS, URL_MAIN, URL_LOCATIONS} from '../../consts';
import TextInput from '../../components/TextInput';
import SelectCategoryContainer from '../../containers/SelectCategory';
import {Container, Button} from 'react-bootstrap';
import AddOrEditBtnContainer from '../../containers/AddOrEditBtn';
import {Link} from 'react-router-dom';
import './AddOrEditItem.css';


class AddOrEditItem extends React.Component {
    constructor (props) {
        super(props);
        if (props.itemType === LOCATIONS) {
            this.state = {
                name: '',
                address: '',
                Coordinates: {lat: '', lng: ''},
                Category: {name: ''}
            }
        } else {
            this.state = {
                name: '',
            }
        }
    }
    render () {
        const ActionType = this.props.actionType;
        const ItemType = this.props.itemType;
        return (
            <Container>
                <TextInput
                    id='txt-item-name'
                    label={ItemType === CATEGORIES ? 'Category Name' : 'Location Name'}
                    placeholder='Enter name'
                    value={this.state.name}
                    onChange={e => this.setState({name: e.currentTarget.value})}
                />
                {ItemType === LOCATIONS ? 
                    <SelectCategoryContainer
                        id='select-item-category'
                        label='Location Category'
                        value={this.state.Category.name}
                        onChange={e => this.setState({Category: {name: e.currentTarget.value}})}
                    />
                : null}
                {ItemType === LOCATIONS ?
                    <TextInput
                        id='txt-location-address'
                        label='Location Address'
                        value={this.state.address}
                        placeholder='Enter address'
                        onChange={e => this.setState({address: e.currentTarget.value})}
                    />
                : null}
                {ItemType === LOCATIONS ?
                    <div className='form-row'>
                        <TextInput
                            id='txt-location-lat'
                            label='Latitude'
                            value={isNaN(this.state.Coordinates.lat) ? '' : this.state.Coordinates.lat}
                            placeholder='Enter latitude'
                            onChange={e => {
                                const CurrentTarget = e.currentTarget;
                                this.setState(prevState => {
                                    prevState.Coordinates.lat = CurrentTarget.value;

                                    return prevState
                                });
                            }}
                        />
                        <TextInput
                            id='txt-location-lng'
                            label='Longitude'
                            value={isNaN(this.state.Coordinates.lng) ? '' : this.state.Coordinates.lng}
                            placeholder='Enter longitude'
                            onChange={e => {
                                const CurrentTarget = e.currentTarget;
                                this.setState(prevState => {
                                    prevState.Coordinates.lng = CurrentTarget.value;

                                    return prevState
                                });
                            }}
                        />
                    </div>
                : null}

                <AddOrEditBtnContainer
                    btnText={ActionType === ADD_ITEM ? 'Add' : 'Update'}
                    itemType={ItemType}
                    item={this.state}
                    actionType={ActionType}
                />
                
                <Link
                    id='add-or-edit-item-back'
                    to={ItemType === CATEGORIES ? URL_MAIN: URL_LOCATIONS}
                >
                    <Button variant="outline-secondary">back</Button>
                </Link>
                
                <div id='err-div' style={{color: 'red'}}></div>
            </Container>
        );
    }
}

AddOrEditItem.defaultProps = {
    actionType: ADD_ITEM,
    itemType: CATEGORIES
}

export default AddOrEditItem