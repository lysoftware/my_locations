import React from 'react';
import './AppWrapper.css';
import {
    CATEGORIES, 
    LOCATIONS, 
    URL_LOCATIONS,
    URL_MAIN, // categories view
    URL_ADD_CATEGORY,
    URL_ADD_LOCATION
} from '../../consts';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

const AppWrapper = props => {
    const GetAddItemUrl = () => {
        switch (props.listType) {
            case CATEGORIES:
                return URL_ADD_CATEGORY;
            case LOCATIONS:
                return URL_ADD_LOCATION;
            default:
                return URL_MAIN
        }
    }
    return (
        <div id='AppWrapper'>
            <div id='header' className='d-flex align-items-center'>
                <Container>
                    <Row>
                        <Col className='d-flex justify-content-center'>
                            <Link to={GetAddItemUrl()}>
                                <Button>
                                    add
                                </Button>
                            </Link>
                        </Col>
                        <Col id='screen-title' className='d-flex justify-content-center align-items-center'>
                            {props.listType === CATEGORIES ? 'Manage Categories' : 'Manage Locations'}
                        </Col>
                        <Col className='d-flex justify-content-center'></Col>
                    </Row>
                </Container>
            </div>
            {props.children}
            <div id='footer' className='d-flex align-items-center'>
                <Container>
                    <Row>
                        <Col className='d-flex justify-content-center'>
                            <Link to={URL_MAIN}>
                                <Button
                                    style={{backgroundColor: props.listType === LOCATIONS ? 'unset' : '#9ed262'}}
                                >
                                    CATEGORIES
                                </Button>
                            </Link>
                            <Link to={URL_LOCATIONS}>
                                <Button
                                    style={{backgroundColor: props.listType === LOCATIONS ? '#9ed262' : 'unset'}}
                                >
                                    LOCATIONS
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default AppWrapper;