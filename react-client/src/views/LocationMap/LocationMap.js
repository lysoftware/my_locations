import React from 'react';
import {Map, GoogleApiWrapper, Marker} from 'google-maps-react';
import {Container, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {URL_LOCATIONS} from '../../consts';
import './LocationMap.css';
import {connect} from 'react-redux';


class LocationMap extends React.Component {
    render () {
        return (
            <Container id='LocationMap'>
                <div id='map-container'>
                    <Map
                        google={this.props.google}
                        zoom={8}
                        style={{width: '100%', height: '100%'}}
                        initialCenter={this.props.Coordinates}
                    >
                        <Marker
                            position={this.props.Coordinates}
                        />
                    </Map>
                </div>
                <div id='map-back-div'>
                    <Link
                        to={URL_LOCATIONS}
                    >
                        <Button>
                            back
                        </Button>
                    </Link>
                </div>
            </Container>
        );
        
    }
}

const MapStateToProps = state => {
    return {
        Coordinates: state.CurrentLocation.Coordinates
    }
}

const ConnectedLocationMap = connect(MapStateToProps)(LocationMap);

export default GoogleApiWrapper({apiKey: 'API_KEY_HERE'})(ConnectedLocationMap);

