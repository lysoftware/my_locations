import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {URL_LOCATIONS} from '../../consts';
import './LocationDetails.css';

const MapStateToProps = state => {
    let name = '', address = '', lat = '', lng = '', categoryName = '';
    if (state.CurrentLocation) {
        name = state.CurrentLocation.name;
        address = state.CurrentLocation.address;
        lat = state.CurrentLocation.Coordinates.lat;
        lng = state.CurrentLocation.Coordinates.lng;
        categoryName = state.CurrentLocation.Category.name; 
    } 

    return {
        name: name,
        address: address,
        lat: lat,
        lng: lng,
        categoryName: categoryName
    }
}


const LocationDetailsView = props => {
    return (
        <Container
            id='LocationDetailsView'
        >
            <Row className='item'>
                <Col xs={12} sm={3}>
                    Name: 
                </Col>
                <Col xs={12} sm={9}>
                    {props.name}
                </Col>
            </Row>
            <Row className='item'>
                <Col xs={12} sm={3}>
                    Address: 
                </Col>
                <Col xs={12} sm={9}>
                    {props.address}
                </Col>
            </Row>
            <Row className='item'>
                <Col xs={12} sm={3}>
                    Category:   
                </Col>
                <Col xs={12} sm={9}>
                    {props.categoryName}
                </Col>
            </Row>
            <Row className='item'>
                <Col xs={12} sm={3}>
                    Coordinates:   
                </Col>
                <Col xs={12} sm={9}>
                    Latitude: {props.lat.toString().slice(0,4)}, Longitude: {props.lng.toString().slice(0,4)}
                </Col>
            </Row>
            <Row>
                <Col>
                    <Link to={URL_LOCATIONS}>
                        <Button>
                            back
                        </Button>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}

export default connect(MapStateToProps)(LocationDetailsView);