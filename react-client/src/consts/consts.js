// ------- CATEGORIES --------
export const RESTAURANTS = 'Restaurants';
export const CLUBS = 'Night Clubs';
export const RESIDENTIAL = 'Residential';
export const PARKS = 'Parks';
export const SHOPPING_MALLS = 'Shopping Centers';


export const NEW_YORK = 'New York';

// ------- ListView Types --------
export const CATEGORIES = 'CATEGORIES';
export const LOCATIONS = 'LOCATIONS';

// ------- Router Url's --------
export const URL_MAIN = './'; // this is the categories view
export const URL_LOCATIONS = './locations';
export const URL_ADD_CATEGORY = './add-category';
export const URL_ADD_LOCATION = './add-location';
export const URL_EDIT_CATEGORY = './edit-category';
export const URL_EDIT_LOCATION = './edit-location';
export const URL_LOCATION_DETAILS = './location-details';
export const URL_LOCATION_MAP = './location-map';


// ------- available actions on AddOrEditItemView --------
export const ADD_ITEM = 'ADD_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';


// ------- localStorage Prefixes --------
export const CATEGORY_LOCAL_STORAGE_ITEM_PREFIX = '_cat-';
export const LOCATION_LOCAL_STORAGE_ITEM_PREFIX = '_loc-';
export const CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX = '_cur-cat';
export const CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX = '_cur-loc';