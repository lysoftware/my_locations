import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import './SingleCategory.css';
import {isSelectedItem} from '../../utils';
import {Link} from 'react-router-dom';
import {URL_EDIT_CATEGORY} from '../../consts';


const SingleCategory = props => {
    const GetClassName = () => {
        let res = 'SingleCategory d-flex justify-content-between';
        if (isSelectedItem(props.CurrentCategory, props.name)) {
            res += ' selected';
        }

        return res;
    }
    return (
        <Row className={GetClassName()} onClick={props.selectCategory}>
            <Col>{props.name}</Col>
            <Col>
                <Container>
                    <Row>
                        <Col className='d-flex justify-content-end'>
                            <Button className='btn-sm category-btn'>
                                <Link
                                    to={URL_EDIT_CATEGORY}
                                    onClick={props.selectCategory}
                                >
                                    edit
                                </Link>
                            </Button>
                        </Col>
                        <Col className='d-flex justify-content-end margin-1p'>
                            <Button 
                                className='btn-sm category-btn'
                                onClick={props.removeCategory}
                            >
                                remove
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </Col>
        </Row>
    );
}

export default SingleCategory