import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

const RouteSelector = props => {
    return (
        <Container className='RouteSelector'>
            <Row>
                {props.urlOptions.map((item, i) => (
                    <Col 
                        xs={2} sm={1}
                        key={'_url-opt-' + i}
                    >
                        <Link to={item.value} className='route-selector-link'>
                            <Button variant="secondary" size='sm'>
                                {item.label}
                            </Button>
                        </Link>
                    </Col>
                ))}
            </Row>
        </Container>
    );
}

RouteSelector.defaultProps = {
    urlOptions: [
        {value: './url1', label: 'url1'},
        {value: './url2', label: 'url2'},
        {value: './url3', label: 'url3'}
    ]
}

export default RouteSelector;