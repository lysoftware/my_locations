import React from 'react';


const TextInput = props => {
    return (
        <div className="form-group">
            <label htmlFor={props.id}>
                {props.label}
            </label>
            <input 
                type={props.type}
                className="form-control" 
                id={props.id}
                value={props.value}
                placeholder={props.placeholder}
                onChange={props.onChange}
            />
            <small className="form-text text-muted">{props.shortComment}</small>
        </div>
    );
}

TextInput.defaultProps = {
    label: '',
    value: 'defaultValue',
    placeholder: '',
    shortComment: '',
    onChange: e => console.info('default onChange fired'),
    type: 'text'
}

export default TextInput