import React from 'react';


const SelectInput = props => {
    return (
        <div className="form-group">
            <label htmlFor={props.id}>
                {props.label}
            </label>
            <select id={props.id} className="form-control" onChange={props.onChange}>
                <option value=''>Choose</option>
                {props.options.map(item => (
                    <option 
                        key={'_' + item.value}
                        value={item.value}
                    >
                        {item.label}
                    </option>
                ))}
            </select>
        </div>
    );
}

SelectInput.defaultProps = {
    id: '',
    value: '',
    label: '',
    options: [{value: 'defaultVal', label: 'defaultLabel'}],
    onChange: e => console.info('default onChange fired')
}

export default SelectInput;