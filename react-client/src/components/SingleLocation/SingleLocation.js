import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import './SingleLocation.css';
import {isSelectedItem} from '../../utils';
import RouteSelector from '../RouteSelector';
import {URL_LOCATION_DETAILS, URL_LOCATION_MAP, URL_EDIT_LOCATION} from '../../consts';
import {Link} from 'react-router-dom';


class SingleLocation extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            isSelector: false
        }
        this.handleSingleLocationClick = this.handleSingleLocationClick.bind(this);
    }
    handleSingleLocationClick (locationName) {
        this.setState({isSelector: !this.state.isSelector});
        this.props.selectLocation(locationName);
    }
    render () {
        const IsDisplaySelector = this.state.isSelector && isSelectedItem(this.props.CurrentLocation, this.props.name);
        return (
            <Row 
                className='SingleLocation'
                onClick={e => this.handleSingleLocationClick(this.props.name)}
            >
                
                <Col>{this.props.name}</Col>
                <Col>
                    <Container>
                        <Row>
                            <Col className='d-flex justify-content-end'>
                                <Link
                                    onClick={e => this.props.selectLocation(this.props.name)}
                                    to={URL_EDIT_LOCATION}
                                >
                                    <Button className='btn-sm location-btn'>edit</Button>
                                </Link>
                            </Col>
                            <Col className='d-flex justify-content-end margin-1p'>
                                <Button 
                                    className='btn-sm location-btn'
                                    onClick={this.props.removeLocation}
                                >
                                    remove
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Col>
                {IsDisplaySelector ?
                    <RouteSelector
                        urlOptions={[
                            {value: URL_LOCATION_DETAILS, label: 'Details'},
                            {value: URL_LOCATION_MAP, label: 'Map'}
                        ]}
                    />
                : null}
            </Row>
        );
    }
}

export default SingleLocation;