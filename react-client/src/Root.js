import React from 'react';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom';
import LocationsView from './views/LocationsView';
import CategoriesView from './views/CategoriesView';
import AddOrEditItem from './views/AddOrEditItem';
import LocationDetailsView from './views/LocationDetails';
import LocationMap from './views/LocationMap';
import {
    URL_LOCATIONS,
    URL_ADD_CATEGORY, 
    URL_EDIT_CATEGORY,
    ADD_ITEM, CATEGORIES, 
    URL_ADD_LOCATION, 
    LOCATIONS,
    EDIT_ITEM,
    URL_EDIT_LOCATION,
    URL_LOCATION_DETAILS,
    URL_LOCATION_MAP
} from './consts';
import 'bootstrap/dist/css/bootstrap.min.css';
import {getLocalStorageStateIfExist} from './actions';
import {connect} from 'react-redux';


class Root extends React.Component {
    componentDidMount () {
        this.props.getLocalStorageStateIfExist();
    }
    render () {
        return (
            <Router>
                <Switch>
                    <Route 
                        exact 
                        path={'/'} 
                        component={CategoriesView}
                    />
                    <Route 
                        path={URL_LOCATIONS.substring(1, URL_LOCATIONS.length)} 
                        component={LocationsView}
                    />
                    <Route
                        path={URL_ADD_CATEGORY.substring(1, URL_ADD_CATEGORY.length)}
                        component={browserStuff => <AddOrEditItem actionType={ADD_ITEM} itemType={CATEGORIES}/>}
                    />
                    <Route
                        path={URL_ADD_LOCATION.substring(1, URL_ADD_LOCATION.length)}
                        component={browserStuff => <AddOrEditItem actionType={ADD_ITEM} itemType={LOCATIONS}/>}
                    />
                    <Route
                        path={URL_EDIT_CATEGORY.substring(1, URL_EDIT_CATEGORY.length)}
                        component={browserStuff => <AddOrEditItem actionType={EDIT_ITEM} itemType={CATEGORIES}/>}                
                    />
                    <Route
                        path={URL_EDIT_LOCATION.substring(1, URL_EDIT_LOCATION.length)}
                        component={browserStuff => <AddOrEditItem actionType={EDIT_ITEM} itemType={LOCATIONS}/>}                
                    />
                    <Route
                        path={URL_LOCATION_DETAILS.substring(1, URL_LOCATION_DETAILS.length)}
                        component={LocationDetailsView}                
                    />
                    <Route
                        path={URL_LOCATION_MAP.substring(1, URL_LOCATION_MAP.length)}
                        component={LocationMap}                
                    />
                    <Route 
                        path={'*'} 
                        component={browserStuff => <h2>No Match</h2>}
                    />
                </Switch>
            </Router>
        );
    }
}

const MapDispatchToProps = dispatch => {
    return {
        getLocalStorageStateIfExist: () => dispatch(getLocalStorageStateIfExist())
    }
}

export default connect(null, MapDispatchToProps)(Root);