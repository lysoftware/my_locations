import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from './Root';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import MainReducer from './reducers/MainReducer';
import {createStore} from 'redux';

const Store = createStore(MainReducer);

ReactDOM.render(<Provider store={Store}><Root/></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
