import React from 'react';
import {connect} from 'react-redux';
import SingleCategory from '../components/SingleCategory';
import {selectCategory, removeItemFromList} from '../actions';
import {CATEGORIES} from '../consts';


const MapStateToProps = state => {
    return {
        CurrentCategory: state.CurrentCategory
    }
}

const MapDispatchToProps = dispatch => {
    return {
        selectCategory: categoryName => dispatch(selectCategory(categoryName)),
        removeItemFromList: (itemType, itemName) => dispatch(removeItemFromList(itemType, itemName))
    }
}

const CategoryContainer = props => {
    return (
        <SingleCategory
            name={props.name}
            CurrentCategory={props.CurrentCategory}
            selectCategory={e => props.selectCategory(props.name)}
            removeCategory={e => props.removeItemFromList(CATEGORIES, props.name)}
        />
    );
}

export default connect(MapStateToProps, MapDispatchToProps)(CategoryContainer);