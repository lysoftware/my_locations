import React from 'react';
import SingleLocation from '../components/SingleLocation';
import {connect} from 'react-redux';
import {selectLocation, removeItemFromList} from '../actions';
import {LOCATIONS} from '../consts';


const MapStateToProps = state => {
    return {
        CurrentLocation: state.CurrentLocation
    }
}

const MapDispatchToProps = dispatch => {
    return {
        selectLocation: locationName => dispatch(selectLocation(locationName)),
        removeItemFromList: (itemType, itemName) => dispatch(removeItemFromList(itemType, itemName))
    }
}

const SingleLocationContainer = props => {
    return (
        <SingleLocation
            CurrentLocation={props.CurrentLocation}
            selectLocation={props.selectLocation}
            name={props.name}
            removeLocation={e => props.removeItemFromList(LOCATIONS, props.name)}
        />
    );
}

export default connect(MapStateToProps, MapDispatchToProps)(SingleLocationContainer)