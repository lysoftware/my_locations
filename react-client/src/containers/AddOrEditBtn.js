import React from 'react';
import {connect} from 'react-redux';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {addItemToList, editListItem} from '../actions';
import {URL_MAIN, URL_LOCATIONS, LOCATIONS, ADD_ITEM} from '../consts';

const MapStateToProps = state => {
    return {
        CurrentCategoryName: state.CurrentCategory ? state.CurrentCategory.name : '',
        CurrentLocationName: state.CurrentLocation ? state.CurrentLocation.name : ''
    }
}

const MapDispatchToProps = dispatch => {
    return {
        addItemToList: (event, itemType, item) => dispatch(addItemToList(event, itemType, item)),
        editListItem: (event, itemType, item, itemPrevName) => dispatch(editListItem(event, itemType, item, itemPrevName))
    }
}


const AddOrEditBtnContainer = props => {
    const RequestedAction = props.actionType === ADD_ITEM ? props.addItemToList : props.editListItem;
    const ItemName = props.itemType === LOCATIONS ? props.CurrentLocationName : props.CurrentCategoryName;
    return (
        <Link 
            to={props.itemType === LOCATIONS ? URL_LOCATIONS :  URL_MAIN}
            onClick={e => RequestedAction(e, props.itemType, props.item, ItemName)}
        >
            <Button>
                {props.btnText}
            </Button>
        </Link>
    );
}

export default connect(MapStateToProps, MapDispatchToProps)(AddOrEditBtnContainer);