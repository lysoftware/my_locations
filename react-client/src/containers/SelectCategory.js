import React from 'react';
import SelectInput from '../components/SelectInput';
import {connect} from 'react-redux';

const MapStateToProps = state => {
    return {
        AllCategories: state.AllCategories
    }
}

const SelectCategoryContainer = props => {
    return (
        <SelectInput
            options={props.AllCategories.map(item => ({value: item.name, label: item.name}))}
            value={props.value}
            label={props.label}
            onChange={props.onChange}
            id={props.id}
        />
    );
}

export default connect(MapStateToProps)(SelectCategoryContainer)