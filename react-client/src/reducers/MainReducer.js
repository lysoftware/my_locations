import {
    CATEGORIES,
    LOCATIONS,
    CATEGORY_LOCAL_STORAGE_ITEM_PREFIX,
    LOCATION_LOCAL_STORAGE_ITEM_PREFIX,
    CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX,
    CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX
} from '../consts';
import {
    SELECT_CATEGORY,
    SELECT_LOCATION,
    ADD_ITEM_TO_LIST,
    REMOVE_ITEM_FROM_LIST,
    EDIT_LIST_ITEM,
    GET_LOCAL_STORAGE_STATE_IF_EXIST
} from '../consts/ActionNames';
import { 
    isItemInList, 
    isValidLocation, 
    isValidCategory,
    isSelectedItem, 
    updateAssociatedLocations,
    addItemToLocalStorage,
    removeItemFromLocalStorage,
    editLocalStorageItem,
    isItemsInLocalStorage,
    verifyIntCooridinates
} from '../utils/utils';

const InitialState = {
    AllCategories: [],
    AllLocations: [],
    CurrentCategory: null,
    CurrentLocation: null
}

const MainReducer = function (state = InitialState, action) {
    const $ = window.jQuery;
    let prevState, itemType, itemName, item, processStatus, validationResult;
    switch (action.type) {
        case SELECT_CATEGORY:
            prevState = Object.assign({}, state);
            const RequestedCategory = prevState.AllCategories.find(obj => obj.name === action.payload);
            if (RequestedCategory) {
                prevState.CurrentCategory = RequestedCategory;
                editLocalStorageItem(
                    CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX,
                    CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX,
                    JSON.stringify(RequestedCategory)
                );
            }
            
            return prevState;
        case SELECT_LOCATION:
            prevState = Object.assign({}, state);
            const RequestedLocation = prevState.AllLocations.find(obj => obj.name === action.payload);
            if (RequestedLocation) {
                prevState.CurrentLocation = RequestedLocation;
                editLocalStorageItem(
                    CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX,
                    CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX,
                    JSON.stringify(RequestedLocation)
                );
            }
            
            return prevState;
        case ADD_ITEM_TO_LIST:
            $('#err-div').text('');
            processStatus = false;
            prevState = Object.assign({}, state);
            itemType = action.payload.itemType;
            item = action.payload.item;
            if (item.Coordinates) {
                item = verifyIntCooridinates(item);
            }
            if (itemType === CATEGORIES) {
                validationResult = isValidCategory(item);
                if (validationResult.status) {
                    if (!isItemInList(prevState.AllCategories, item, 'name')) {
                        prevState.AllCategories.push(action.payload.item);
                        processStatus = true;
                        addItemToLocalStorage(
                            CATEGORY_LOCAL_STORAGE_ITEM_PREFIX + item.name, 
                            JSON.stringify(item)
                        );
                    }
                } else {
                    $('#err-div').text(validationResult.errMasg);
                }
            } else if (itemType === LOCATIONS) {
                if (!isItemInList(prevState.AllLocations, item, 'name')) {
                    validationResult = isValidLocation(item);
                    if (validationResult.status) {
                        prevState.AllLocations.push(item);
                        processStatus = true;
                        addItemToLocalStorage(
                            LOCATION_LOCAL_STORAGE_ITEM_PREFIX + item.name, 
                            JSON.stringify(item)
                        );
                    } else {
                        $('#err-div').text(validationResult.errMasg);
                    }
                } else {
                    $('#err-div').text('cannot add item already exists');
                }
            }

            if (!processStatus) {
                action.payload.event.preventDefault();
            }

            return prevState;
        case REMOVE_ITEM_FROM_LIST:
            prevState = Object.assign({}, state);
            itemType = action.payload.itemType;
            itemName = action.payload.itemName;
            if (itemType === CATEGORIES) {
                const AllCategories = Array.from(prevState.AllCategories);
                prevState.AllCategories = AllCategories.filter(item => item.name !== itemName);
                if (isSelectedItem(prevState.CurrentCategory, itemName)) {
                    //clear CurrentCategory if it's the one removed
                    prevState.CurrentCategory = null;
                    removeItemFromLocalStorage(CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX)
                }
                removeItemFromLocalStorage(CATEGORY_LOCAL_STORAGE_ITEM_PREFIX + itemName);
            } else if (itemType === LOCATIONS) {
                const AllLocations = Array.from(prevState.AllLocations);
                prevState.AllLocations = AllLocations.filter(item => item.name !== itemName);
                if (isSelectedItem(prevState.CurrentLocation, itemName)) {
                    //clear CurrentLocation if it's the one removed
                    prevState.CurrentLocation = null;
                    removeItemFromLocalStorage(CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX);
                }
                removeItemFromLocalStorage(LOCATION_LOCAL_STORAGE_ITEM_PREFIX + itemName);
            }

            return prevState;
        case EDIT_LIST_ITEM:
            $('#err-div').text('');
            processStatus = false;
            prevState = Object.assign({}, state);
            itemType = action.payload.itemType;
            itemName = action.payload.itemPrevName;
            item = action.payload.item;
            if (item.Coordinates) {
                item = verifyIntCooridinates(item);
            }
            if (itemType === CATEGORIES) {
                validationResult = isValidCategory(item);
                if (validationResult.status) {
                    for (let i = 0; i < prevState.AllCategories.length; i++) {
                        if (prevState.AllCategories[i].name === itemName) {
                            prevState.AllCategories[i] = item;
                            prevState.CurrentCategory = item;
                            processStatus = true;

                            // -- repeating for both current item & list item --
                            editLocalStorageItem(
                                CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX, 
                                CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX, 
                                JSON.stringify(item)
                            );
                            editLocalStorageItem(
                                CATEGORY_LOCAL_STORAGE_ITEM_PREFIX + itemName,
                                CATEGORY_LOCAL_STORAGE_ITEM_PREFIX + item.name,
                                JSON.stringify(item)
                            );
                            break;
                        }
                    }
                    prevState.AllLocations = updateAssociatedLocations(Array.from(prevState.AllLocations), item, itemName);
                } else {
                    $('#err-div').text(validationResult.errMasg)
                }
            } else if (itemType === LOCATIONS) {
                validationResult = isValidLocation(item);
                if (validationResult.status) {
                    for (let i = 0; i < prevState.AllLocations.length; i++) {
                        if (prevState.AllLocations[i].name === itemName) {
                            prevState.AllLocations[i] = item;
                            prevState.CurrentLocation = item;
                            processStatus = true;

                            // -- repeating for both current item & list item --
                            editLocalStorageItem(
                                CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX,
                                CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX,
                                JSON.stringify(item)
                            );
                            editLocalStorageItem(
                                LOCATION_LOCAL_STORAGE_ITEM_PREFIX + itemName,
                                LOCATION_LOCAL_STORAGE_ITEM_PREFIX + item.name,
                                JSON.stringify(item)
                            );
                            break;
                        }
                    }
                } else {
                    $('#err-div').text(validationResult.errMasg);
                }
            }

            if (!processStatus) {
                action.payload.event.preventDefault();
            }

            return prevState;
        case GET_LOCAL_STORAGE_STATE_IF_EXIST:
            prevState = Object.assign({}, state);
            if (isItemsInLocalStorage()) {
                let localStorageCategories = [], localStorageLocations = [];
                for (let i = 0; i < window.localStorage.length; i++) {
                    let key = window.localStorage.key(i), value = window.localStorage.getItem(key);
                    if (key === CURRENT_CATEGORY_LOCAL_STORAGE_ITEM_PREFIX) {
                        prevState.CurrentCategory = JSON.parse(value);
                    } else if (key === CURRENT_LOCATION_LOCAL_STORAGE_ITEM_PREFIX) {
                        prevState.CurrentLocation = JSON.parse(value);
                    } else if (key.startsWith(CATEGORY_LOCAL_STORAGE_ITEM_PREFIX)) {
                        localStorageCategories.push(JSON.parse(value));
                    } else if (key.startsWith(LOCATION_LOCAL_STORAGE_ITEM_PREFIX)) {
                        localStorageLocations.push(JSON.parse(value));
                    }
                }

                if (localStorageCategories.length) {
                    prevState.AllCategories = localStorageCategories;
                }
                if (localStorageLocations.length) {
                    prevState.AllLocations = localStorageLocations;
                }
            }

            return prevState;
        default:
            return state;
    }
}

export default MainReducer;

