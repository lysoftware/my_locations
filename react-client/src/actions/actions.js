import {
    SELECT_CATEGORY,
    SELECT_LOCATION,
    ADD_ITEM_TO_LIST,
    REMOVE_ITEM_FROM_LIST,
    EDIT_LIST_ITEM,
    GET_LOCAL_STORAGE_STATE_IF_EXIST
} from '../consts/ActionNames';


export function selectCategory (categoryName) {
    return {
        type: SELECT_CATEGORY,
        payload: categoryName
    }
}

export function selectLocation (locationName) {
    return {
        type: SELECT_LOCATION,
        payload: locationName
    }
}

export function addItemToList (event, itemType, item) {
    return {
        type: ADD_ITEM_TO_LIST,
        payload: {
            event: event,
            itemType: itemType,
            item: item
        }
    }
}

export function removeItemFromList (itemType, itemName) {
    return {
        type: REMOVE_ITEM_FROM_LIST,
        payload: {
            itemType: itemType,
            itemName: itemName
        }
    }
}

export function editListItem (event, itemType, item, itemPrevName) {
    return {
        type: EDIT_LIST_ITEM,
        payload: {
            event: event,
            itemType: itemType,
            itemPrevName: itemPrevName,
            item: item
        }
    }
}

export function getLocalStorageStateIfExist () {
    return {
        type: GET_LOCAL_STORAGE_STATE_IF_EXIST
    }
}