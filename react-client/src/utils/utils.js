export function getLocationsByCategory (currentCategory, locationsList) {
    if (!currentCategory) return [];
    return locationsList.filter(item => item.Category.name === currentCategory.name);
}

export function sortLocationsByName (unsortedLocations) {
    return unsortedLocations.sort((a, b) => {
        const NAME_A = a.name.toLowerCase();
        const NAME_B = b.name.toLowerCase();
        if (NAME_A > NAME_B) return 1;
        if (NAME_A < NAME_B) return -1;
        else return 0;
    });
}


export function isSelectedItem (currentSelectedItem, singleItemName) {
    let res = false;
    if (currentSelectedItem) {
        if (currentSelectedItem.name === singleItemName) {
            res = true;
        }
    }

    return res;
}

export function isItemInList (list, item, lookupKey) {
    let res = false;
    const RequestedItem = list.find(obj => obj[lookupKey] === item[lookupKey]);
    if (RequestedItem) {
        res = true
    };

    return res;
}

export function isValidLocation (locationItem) {
    const NameRegex = new RegExp(/^[^0-9,.!@#$%^&*()+_\/]{3,30}$/);
    const AddressRegex = new RegExp(/^[^!@#$%^&*()+_\/]{3,100}$/);
    let res = true, errMasg = '';
    if (!NameRegex.test(locationItem.name) && !NameRegex.test(locationItem.Category.name)) {
        res = false;
    } else if (!AddressRegex.test(locationItem.address)) {
        res = false;
    } else if (!locationItem.Coordinates.lat || !locationItem.Coordinates.lng) {
        res = false;
    } else if (isNaN(locationItem.Coordinates.lat) || isNaN(locationItem.Coordinates.lng)) {
        res = false;
    }

    if (!res) errMasg = 'Something is wrong with your inputs, please check and try again';

    return {status: res, errMasg: errMasg}
}

export function isValidCategory (categoryItem) {
    const NameRegex = new RegExp(/^[^0-9,.!@#$%^&*()+_\/]{3,30}$/);
    let res = true, errMasg = '';
    if (!NameRegex.test(categoryItem.name)) {
        res = false;
        errMasg = 'Something is wrong with your input, please check and try again'
    }

    return {status: res, errMasg: errMasg};
}

export function updateAssociatedLocations (locationsArr, itemToUpdate, prevItemName) {
    locationsArr.forEach(location => {
        if (location.Category.name === prevItemName) {
            location.Category = itemToUpdate;
        }
    });

    return locationsArr;
}

export function addItemToLocalStorage (key, value) {
    const NoSpacesKey = key.replace(/ /g, '').toLowerCase();
    if (!window.localStorage.getItem(NoSpacesKey)) {
        window.localStorage.setItem(NoSpacesKey, value);
    }
}

export function removeItemFromLocalStorage (key) {
    const NoSpacesKey = key.replace(/ /g, '').toLowerCase();
    window.localStorage.removeItem(NoSpacesKey);
}

export function editLocalStorageItem (oldKey, newKey, newVal) {
    const NoSpacesOldKey = oldKey.replace(/ /g, '').toLowerCase();
    const NoSpacesNewKey = newKey.replace(/ /g, '').toLowerCase();
    window.localStorage.removeItem(NoSpacesOldKey);
    window.localStorage.setItem(NoSpacesNewKey, newVal);
}

export function isItemsInLocalStorage () {
    if (localStorage.length) return true;
    else return false;
}

export function verifyIntCooridinates (locationItem) {
    const Lat = parseFloat(locationItem.Coordinates.lat);
    const Lng = parseFloat(locationItem.Coordinates.lng);
    locationItem.Coordinates = {lat: Lat, lng: Lng};
    return locationItem;
}