const EXPRESS = require('express'), BODY_PARSER = require('body-parser'), PATH = require('path');
const FS = require('fs');

const App = EXPRESS(); // initializing app with express server software

// App.use is a method for implementing middleware
App.use(BODY_PARSER.json());
App.use(BODY_PARSER.urlencoded({ extended: true }));
App.use(EXPRESS.static(PATH.join(__dirname, 'react-client/build'))); // serving static files from html document

const Server = App.listen(process.env.PORT || 5000, function () {
    let host = Server.address().address, port = Server.address().port;
    console.info('my_locations app listening at http://%s:%s', host, port);
});

App.get('*', function (req, res) {
    res.sendFile(__dirname + '/react-client/build/index.html');
});